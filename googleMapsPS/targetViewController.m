//
//  targetViewController.m
//  googleMapsPS
//
//  Created by Click Labs133 on 11/5/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "targetViewController.h"

@interface targetViewController ()
@property (strong, nonatomic) IBOutlet UIView *destinationView;

@property (strong, nonatomic) IBOutlet UILabel *NameLabel;
@property (strong, nonatomic) IBOutlet UILabel *StateLabel;
@property (strong, nonatomic) IBOutlet UILabel *CityLabel;
@property (strong, nonatomic) IBOutlet UILabel *latitudeLabel;
@property (strong, nonatomic) IBOutlet UILabel *longitudeLabel;
@property (strong, nonatomic) IBOutlet UILabel *permanentAddressLabel;
@property (strong, nonatomic) IBOutlet UILabel *nameLabelValue;
@property (strong, nonatomic) IBOutlet UILabel *stateLabelValue;
@property (strong, nonatomic) IBOutlet UILabel *cityLabelValue;
@property (strong, nonatomic) IBOutlet UILabel *latitudeLabelValue;
@property (strong, nonatomic) IBOutlet UILabel *longitudeLabelValue;
@property (strong, nonatomic) IBOutlet UILabel *addressLabelValue;

@end

@implementation targetViewController
@synthesize destinationView;
@synthesize nameLabelValue;
@synthesize cityLabelValue;
@synthesize stateLabelValue;
@synthesize latitudeLabelValue;
@synthesize longitudeLabelValue;
@synthesize addressLabelValue;
@synthesize dataInDestination;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    nameLabelValue.text=[dataInDestination objectAtIndex:1];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
