//
//  ViewController.m
//  googleMapsPS
//
//  Created by Cli16 on 11/4/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"

CLLocationCoordinate2D position;
CLLocationManager *manager;

CLGeocoder *geocoder;

CLPlacemark *placemark;
GMSMarker *marker;
NSMutableArray *city;

NSMutableArray *dataInDestination;

NSMutableArray *address;

CLPlacemark *placemark;

NSString *name;

NSDictionary *addressDictionary;

NSString *state;

NSString *cityName;

NSString *country;






@interface ViewController ()
@property (strong, nonatomic) IBOutlet GMSMapView *mapView;
@property (strong, nonatomic) IBOutlet UILabel *GoogleMapsLabel;
@property (strong, nonatomic) IBOutlet UIButton *getDetailButton;


@end

@implementation ViewController
@synthesize mapView;
@synthesize GoogleMapsLabel;
@synthesize getDetailButton;




- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    manager=[[CLLocationManager alloc]init];
    
    manager.delegate = self;
    
    manager.desiredAccuracy=kCLLocationAccuracyBestForNavigation;
    
    [manager requestAlwaysAuthorization];
    
    geocoder=[[CLGeocoder alloc]init];
    
    placemark=[[CLPlacemark alloc]init];
    
    mapView.myLocationEnabled = YES;
    [manager startUpdatingLocation];

    
}





- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation

{


    
    NSLog(@"didUpdateToLocation:%@",newLocation);
    
    CLLocation *currentLocation=newLocation;
    
    if (currentLocation!=nil)
        
    {
 
 
 position = currentLocation.coordinate;
 
 marker = [GMSMarker markerWithPosition:position];
 
 marker.title = @"current loation";
        
 
 marker.map = mapView;
 
        marker.icon = [GMSMarker markerImageWithColor:[UIColor redColor]];
    
    
    }
    [geocoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error)
     
     {
         
         
         
         CLPlacemark *myPlacemark = [placemarks objectAtIndex:0];
         
         NSString *countryCode = myPlacemark.ISOcountryCode;
         
         NSString *countryName = myPlacemark.country;
         
         NSString *cityName= myPlacemark.subAdministrativeArea;
         
         [city addObject:[NSString stringWithFormat:@"%@",cityName]];
         
         
         
         NSString *stateName= myPlacemark.administrativeArea;
         
         [city addObject:[NSString stringWithFormat:@"%@",stateName]];
         
         
         
         NSString *area= myPlacemark.country;
         
         [city addObject:[NSString stringWithFormat:@"%@",area]];
         
         
         
         NSLog(@"My country code:" @"%@" @"and countryName:" @"%@" @"MyCity:" @"%@", countryCode, countryName, cityName);
         
         NSDictionary *addressDictionary =
         
         myPlacemark.addressDictionary;
         
         NSString *show= addressDictionary[@"FormattedAddressLines"];
         
         [city addObject:show];
         
         NSLog(@"%@",show);
         
         
         
     }];
    
}





-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([[segue identifier]isEqualToString:@"join"]) {
        targetViewController *t = segue.destinationViewController;
        //  t.demoDest = demoCurrent;
       // t.arrayTarget=array1;
    
       
        
        t.dataInDestination=city;
        

    
    
    }
    
}

-(void)location



{
    
    
    
    geocoder = [[CLGeocoder alloc] init];
    
        CLLocation *newLocation = [[CLLocation alloc]initWithLatitude:position.latitude
                               
                                                        longitude:position.longitude];
       [geocoder reverseGeocodeLocation:newLocation
     
     
     
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       
                       
                       
                       
                       
                       
                       
                       if (error) {
                           
                           
                           
                           NSLog(@"Geocode failed with error: %@", error);
                           
                           
                           
                           return;
                           
                           
                           
                       }
                       
                       
                       
                       
                       
                       
                       
                       if (placemarks && placemarks.count > 0)
                           
                           
                           
                       {
                           
                           
                           
                           CLPlacemark *placemark = placemarks[0];
                           
                                                      NSDictionary *addressDictionary =
                           
                           
                           
                           placemark.addressDictionary;
                           
                           
                           
                           
                           
                           
                           
                           NSLog(@"%@ ", addressDictionary) ;
                           
                           
                           
                           
                           
                           
                           
                       }
                       
                       
                       
                   }];}


 
 
 
   /* // Reverse Geocoding
    
    NSLog(@"Resolving the Address");
    
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        
        NSLog(@"Found placemarks: %@, error: %@", placemarks);
        
        if (error == nil && [placemarks count] > 0) {
            
            // placemark = [placemarks lastObject];
            
            //    NSLog(@"placemark:%@",placemarks);
            
            addressLabel.text = [NSString stringWithFormat:@"%@ %@\n%@ %@\n%@\n%@",
                                 
                                 placemark.subThoroughfare, placemark.thoroughfare,
                                 
                                 placemark.postalCode, placemark.locality,
                                 
                                 placemark.administrativeArea,
                                 
                                 placemark.country];
            
        } else {
            
            NSLog(@"%@", error.debugDescription);
            
        }
        
    } ];
    
    
    
    /*   [geocoder reverseGeocodeLocation :nlocation
     
     
     
     completionHandler:^(NSArray *placemarks, NSError *error)
     
     
     
     {
     
     
     
     
     
     
     
     if (error) {
     
     
     
     NSLog(@"Geocode failed with error: %@", error);
     
     
     
     return;
     
     
     
     }
     
     
     
     
     
     
     
     if (placemarks && placemarks.count > 0)
     
     
     
     {

     
     //[googlemap clear];
     
     
     
     CLPlacemark *placemark = placemarks[0];
     
     NSDictionary *addressDictionary =
 
     placemark.addressDictionary;

     
     NSArray *show= addressDictionary[@"FormattedAddressLines"];
 
     country = [show componentsJoinedByString:@"," ] ;
     
     
     
     NSLog(@"%@",country);
     
     GMSMarker *markerall = [GMSMarker markerWithPosition:newLocation];
     
     
     
     markerall.title = country;
     
     
     
     markerall.map = googlemap;
     
     
     
     markerall.icon = [GMSMarker markerImageWithColor:[UIColor blueColor]];
     
     
     
     }*/
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    
    
    
    
    
    
    
    


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
